


def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


current_board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for turn in range(10):
    print_board(current_board)
    if turn < 9:
        response = input("Where would " + current_player + " like to move? ")
        space_number = int(response)
        current_board[space_number - 1] = current_player

        if (current_board[0] == current_board[1]) & (current_board[0] == current_board[2]):
            print_board(current_board)
            print(current_board[0] + " wins!")
            exit()

        if (current_board[3] == current_board[4]) & (current_board[3] == current_board[5]):
            print_board(current_board)
            print(current_board[3] + " wins!")
            exit()

        if (current_board[6] == current_board[7]) & (current_board[6] == current_board[8]):
            print_board(current_board)
            print(current_board[6] + " wins!")
            exit()

        if (current_board[0] == current_board[3]) & (current_board[0] == current_board[6]):
            print_board(current_board)
            print(current_board[0] + " wins!")
            exit()

        if (current_board[1] == current_board[4]) & (current_board[1] == current_board[7]):
            print_board(current_board)
            print(current_board[1] + " wins!")
            exit()

        if (current_board[2] == current_board[5]) & (current_board[2] == current_board[8]):
            print_board(current_board)
            print(current_board[2] + " wins!")
            exit()

        if (current_board[0] == current_board[4]) & (current_board[0] == current_board[8]):
            print_board(current_board)
            print(current_board[0] + " wins!")
            exit()

        if (current_board[2] == current_board[4]) & (current_board[2] == current_board[6]):
            print_board(current_board)
            print(current_board[2] + " wins!")
            exit()

        if current_player == "X":
            current_player = "O"
        else:
            current_player = "X"

    else:
        print("Tie!")



